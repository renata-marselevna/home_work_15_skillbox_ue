﻿

#include <iostream>


int function(int N, int x)  //x=0 для четных и х=1 для нечетных
{
    for (int i = 0; i < N; ++i)
    {
        if (i % 2 == x)
        {
            std::cout << i << ' ';
        }
    }
    return 0;
}

int main()
{
    std::cout << "first part of the homework:" << "\n";
    const int N = 25;
    for (int i =0; i<N; ++i)
    {
        if (i % 2 == 0)
        {
            std::cout << i << ' ';
       }
    }

    std::cout << "\n"<<"second part of the homework - function test:" << "\n";
    function(10, 1);

    return 0;
}

